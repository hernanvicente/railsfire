module Railsfire
  class AssociationCollection  < BasicObject

    def initialize(klass, records=[], owner)
      @klass  = klass
      @owner = owner
      @records = records
    end

    def all
      @records
    end

    def new(args, &block)
      @klass.new(args.merge(parent: @owner), &block)
    end
    alias build new

    def find_by(args)
      result = where(args)
      result.any? ? result.first : nil
    end

    def where(args)
      if args && @records.any?
        keys = args.keys
        @records.map do |record|
          record if keys.map { |key| record.send(key.to_sym) == args[key] }.reduce(:&)
        end.compact
      end
    end

    private

      def method_missing(method, *args, &block)
        @records.send(method, *args, &block)
      end

  end
end
