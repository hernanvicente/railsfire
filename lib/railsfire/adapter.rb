module Railsfire
  class Adapter

    def initialize
      @conn = connection
    end

    def create(path, query={})
      normalize(@conn.push(path, query))
    end

    def delete(path, query={})
      normalize(@conn.delete(path, query))
    end

    def copy(from_path, to_path, id)
      o = @conn.get("#{from_path}/#{id}")
      if o
        o = normalize o
        response = @conn.set("#{to_path}/#{id}", o)
        normalize response
      end
    end

    def get(path, query={})
      id = path.split('/').last
      normalize(@conn.get(path, query), id: id)
    end

    def move(from_path, to_path, id)
      m = copy(from_path, to_path, id)
      delete("#{from_path}/#{id}"); true if m
    end

    def query(path, query)
      k, v = query.keys.first.to_s, check_type(query.values.first)
      query = { :orderBy => '"'+k+'"', :startAt => v, :endAt => v }
      response = @conn.get(path, query)
      normalize response
    end

    def update(path, query={})
      updated = @conn.update(path, query)
      if updated
        response = @conn.get(path, query)
        normalize response
      end
    end

    private

      def connection
        Firebase::Client.new(ENV['FIREBASE_NAMESPACE'], ENV['FIREBASE_SECRET_KEY'])
      end

      def check_type(value)
        case value
        when !!value
          value
        when Integer
          value
        else
          '"'+value+'"'
        end
      end

      def normalize(response, **options)
        if response.body
          c = response.body.merge!(options).deep_symbolize_keys
          caller[0][/`.*'/][1..-2] == 'create' ? sanitize(c) : c
        end
      end

      def sanitize(hash)
        hash[:id] = hash.delete(:name)
        hash
      end

  end
end
