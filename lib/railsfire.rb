require 'active_model'
require 'firebase'
require 'railsfire/version'
require 'railsfire/adapter'
require 'railsfire/association_collection'

module Railsfire

  class Base
    include ActiveModel::Model

    attr_accessor :id, :created_at, :updated_at

    @@connection = Adapter.new

    def initialize(attributes = {})
      @parent = attributes.delete(:parent)
      purge_attributes(attributes).each { |k| send("#{k}=", attributes[k]) }
    end

    def self.rf_keys(*key_names)
      if key_names.length > 0
        @attributes = key_names
        @attributes.each do |key|
          send(:attr_accessor, key)
        end
      end
      @attributes
    end

    def self.embed(associations)
      @associations = associations
      getter(associations)
      setter(associations)
    end

    def self.associations
      @associations
    end

    def self.create(values = {})
      values.merge!({created_at: Time.zone.now, updated_at: Time.zone.now})
      if response = adapter.create(self.model_name.plural, values)
        new(response.merge(values)).send(:instance_associations)
      end
    end

    def self.find(id)
      if response = adapter.get("#{self.model_name.plural}/#{id}")
        new(response).send(:instance_associations)
      end
    end

    def self.where(query)
      if query && (response = adapter.query(self.model_name.plural, query))
        response.map { |k,v| new(v.merge(id: k)).send(:instance_associations)}
      end
    end

    def self.find_by(query)
      if query && (response = adapter.query(self.model_name.plural, query))
        c = response.map { |k,v| new(v.merge(id: k))}.compact.first
        c.send(:instance_associations) if c
        c
      end
    end

    def parent
      @parent
    end

    def update(query)
      if query && (response = adapter.update(build_path, query))
        o = response.merge(id: self.id)
        # update_attributes(o)
        purge_attributes(o).each { |k| send("#{k}=", o[k]) }
        self
      end
    end

    def destroy
      adapter.delete(build_path)
      # if parent
      #   arr = self.parent.send("#{self.model_name.plural}")
      #   arr.delete_if{|e| e.id == self.id}
      # end
    end

    def save
      if self.id
        # call update()
      else
        if response = adapter.create(build_path, self.to_hash)
          update_attributes(response).send(:instance_associations)
        end
      end
    end

    # Move to utils.rb
    def build_path
      # Check if parent is persisted
      path_objects = ancestors(self).reverse
      path = ''
      path_objects.each do |path_object|
        path += "/#{path_object.model_name.plural}/#{path_object.id}"
      end
      path += "/#{self.model_name.plural}"
      path += "/#{self.id}" if self.id
      path
    end

    # Move to utils.rb
    def ancestors(ancestors=[], descendant)
      if descendant.parent.nil?
        return ancestors
      else
        ancestors << descendant.parent
        ancestors(ancestors, descendant.parent)
      end
    end

    # Move to utils.rb
    def to_hash
      self.class.rf_keys.inject({}) do |hash, key|
        hash.merge({ key => self.send(key) })
      end
    end


    private

      def self.adapter
        @adapter ||= @@connection
      end

      def adapter
        self.class.adapter
      end

      def self.attributes
        @attributes
      end

      def purge_attributes(attributes)
        attrs = [:id, :created_at, :updated_at] + self.class.attributes
        attrs += self.class.associations if self.class.associations.present?
        attributes.present? ? attrs & attributes.keys : attrs
      end

      def update_attributes(new_values)
        new_values.each do |k, v|
          self.send("#{k.to_sym}=", v)
        end
        self
      end

      def self.getter(args)
        args.each do |method|
          define_method(method) do
            instance_variable_get("@#{method}")
          end
        end
      end

      def self.setter(args)
        args.each do |method|
          define_method("#{method}=") do |value|
            association = method.to_s.singularize.classify.constantize
            values = value.map{ |k, v| association.new(v.merge({ id: k, parent: self })) }
            association_collection = AssociationCollection.new(association, values, self)
            instance_variable_set("@#{method}", association_collection)
          end
        end
      end

      def instance_associations
        associations = self.class.associations
        if !associations.nil?
          associations.each do |a|
            if self.send("#{a.to_s}").nil?
              association = a.to_s.singularize.classify.constantize
              association_collection = AssociationCollection.new(association, [], self)
              self.send("#{a.to_s}=", association_collection)
            end
          end
        end
        self
      end

  end
end
